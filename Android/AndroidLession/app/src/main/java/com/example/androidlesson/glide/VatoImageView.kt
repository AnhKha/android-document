package com.example.androidlesson.glide

import android.content.Context
import android.databinding.BindingAdapter
import android.support.annotation.NonNull
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.androidlesson.R
import com.example.androidlesson.custom_view.ImageLoader

class VatoImageView(context: Context?) : AppCompatImageView(context) {
    private var mCircleTransform: Boolean = false

    constructor(context: Context?, attr: AttributeSet) : this(context) {
        val a = context!!.obtainStyledAttributes(attr, R.styleable.HasakiImageView, 0, 0)
        mCircleTransform = a.getBoolean(R.styleable.HasakiImageView_circleTransform, false)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    /*@BindingAdapter("imageUrl")
    fun setImageUrl(@NonNull view: VatoImageView, @NonNull imageUrl: String) {

        if (view.mCircleTransform) {
            Glide.with(view.context)
                    .load(imageUrl)
                    .centerCrop()
                    .transform(ImageLoader.CircleTransform(view.context))
                    .into(view)
        } else {
            Glide.with(view.context)
                    .load(imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .into(view)
        }
    }*/

    companion object {
        @BindingAdapter("imageUrl")
        @JvmStatic
        fun setImageUrl(@NonNull view: VatoImageView, @NonNull imageUrl: String) {

            if (view.mCircleTransform) {
                Glide.with(view.context)
                        .load(imageUrl)
                        .centerCrop()
                        .transform(ImageLoader.CircleTransform(view.context))
                        .into(view)
            } else {
                Glide.with(view.context)
                        .load(imageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter()
                        .into(view)
            }
        }
    }
}