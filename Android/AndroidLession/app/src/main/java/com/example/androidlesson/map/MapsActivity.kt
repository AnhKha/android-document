package com.example.androidlesson.map

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.androidlesson.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.maps_activity)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val location = LatLng(10.7622638, 106.6764709)
        mMap.addMarker(MarkerOptions().position(location).title("Marker in HCM"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 14f))
        val polyline= PolylineOptions()
        polyline.add(LatLng(10.7622638, 106.6764709))
        polyline.add(LatLng(10.7666296, 106.6796626))
        polyline.add(LatLng(10.7666296, 106.6796626))
        mMap.addPolyline(polyline)
    }
}
