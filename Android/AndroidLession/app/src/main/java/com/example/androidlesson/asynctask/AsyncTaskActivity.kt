package com.example.androidlesson.asynctask

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.example.androidlesson.R


class AsyncTaskActivity : AppCompatActivity() {
    private lateinit var btnStartAsync: Button
    private lateinit var myAsyncTask: AsyncTaskDemo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.asynctask_activity_layout)
        btnStartAsync = findViewById(R.id.btnStartAsync)
        btnStartAsync.setOnClickListener {
            //Khởi tạo tiến trình của bạn
            //Truyền Activity chính là MainActivity sang bên tiến trình của mình
            myAsyncTask = AsyncTaskDemo(this)
            //Gọi hàm execute để kích hoạt tiến trình
            myAsyncTask.execute()
        }
    }
}