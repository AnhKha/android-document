package com.example.androidlesson.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface APIService {
    @GET("hound/list")
    fun getListDog(): Call<Model>

    @GET("hound/{path}/images")
    fun getListDogImage(@Path("path") path: String): Call<Model>

}