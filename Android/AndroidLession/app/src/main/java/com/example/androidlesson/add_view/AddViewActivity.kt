package com.example.androidlesson.add_view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.TextView
import com.example.androidlesson.R
import com.example.androidlesson.R.layout.dynamic_view_activity
import kotlinx.android.synthetic.main.add_view_row.view.*
import kotlinx.android.synthetic.main.dynamic_view_activity.*


class AddViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(dynamic_view_activity)
        btnAddDynamicView.setOnClickListener { addViewLayout() }
    }

    private fun addDynamicView() {
        val text = edtContent.text.toString()
        val lparams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val textView = TextView(this)
        textView.layoutParams = lparams
        textView.text = text
        textView.textSize = 20.0f
        llAddDynamicView.addView(textView)
    }

    private fun addViewLayout() {
        val rowView = layoutInflater.inflate(R.layout.add_view_row, null)
        rowView.txtAddViewContent.text = edtContent.text.toString()
        llAddDynamicView.addView(rowView)
    }
}