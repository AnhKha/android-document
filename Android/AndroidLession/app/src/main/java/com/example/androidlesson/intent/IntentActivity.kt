package com.example.androidlesson.intent

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import com.example.androidlesson.R


class IntentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.intent_activity_layout)
        val packageBundle = intent.getBundleExtra("BundlePackage")
        val intentValue = packageBundle.getString("IntentValue")
        var txtValue = findViewById<TextView>(R.id.txtValue)
        var btnBackWithResult = findViewById<Button>(R.id.btnBackWithResult)
        txtValue.text = intentValue
        btnBackWithResult.setOnClickListener { backForResult() }
    }

    private fun backForResult(){
        val intent = intent
        intent.putExtra("IntentForResult", "This is Intent For Result")
        setResult(202, intent)
        finish()
    }
}