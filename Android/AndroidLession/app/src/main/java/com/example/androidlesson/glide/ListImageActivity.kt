package com.example.androidlesson.glide

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import com.bumptech.glide.Glide
import com.example.androidlesson.ListImageBinding
import com.example.androidlesson.R
import com.example.androidlesson.retrofit.APIService
import com.example.androidlesson.retrofit.ApiUtils
import com.example.androidlesson.retrofit.Model
import kotlinx.android.synthetic.main.list_image_activity_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ListImageActivity : AppCompatActivity() {
    lateinit var apiService: APIService
    private var dogName = ""
    lateinit var listImageActivity: ListImageBinding
    companion object {
        var DOG_NAME: String = "DOGNAME"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listImageActivity = DataBindingUtil.setContentView(this, R.layout.list_image_activity_layout)
        listImageActivity.imageUrl = "https://images.dog.ceo/breeds/hound-afghan/n02088094_1007.jpg"
        /*getIntentData()
        apiService = ApiUtils().getAPIService()
        getData()*/
    }

    private fun getData() {
        apiService.getListDogImage(dogName).enqueue(object : Callback<Model> {
            override fun onFailure(call: Call<Model>?, t: Throwable?) {
                Log.v("retrofit", "call failed")
            }

            override fun onResponse(call: Call<Model>?, response: Response<Model>?) {
                if (response?.body() != null && response.body() is Model) {
                    val model: Model = response.body()!!
                    model.message?.let { bindData(it) }
                }
            }

        })
    }

    private fun getIntentData() {
        dogName = intent.getStringExtra(DOG_NAME)
    }

    private fun bindData(list: List<String>) {
        var listDogImageAdapter = ListDogImageAdapter(list.toMutableList(), this)
        rcvListImage.layoutManager = GridLayoutManager(this, 2)
        rcvListImage.adapter = listDogImageAdapter
    }
}