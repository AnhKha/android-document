package com.example.androidlesson

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.androidlesson.add_view.AddViewActivity
import com.example.androidlesson.animation.AnimationActivity
import com.example.androidlesson.asynctask.AsyncTaskActivity
import com.example.androidlesson.broadcast_reciever.BroadCastRecieverActivity
import com.example.androidlesson.content_provider.ContentProviderActivity
import com.example.androidlesson.custom_view.CustomViewActivity
import com.example.androidlesson.intent.IntentActivity
import com.example.androidlesson.map.MapsActivity
import com.example.androidlesson.notification.NotificationActivity
import com.example.androidlesson.retrofit.RetrofitActivity
import com.example.androidlesson.service.ServiceActivity
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private lateinit var btnContentProvider: Button
    private lateinit var btnIntent: Button
    private lateinit var btnIntentForResult: Button
    private lateinit var btnService: Button
    private lateinit var btnAsyncTask: Button
    private lateinit var btnBroadcast: Button
    private lateinit var btnMap: Button
    private lateinit var btnNotification: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
        btnContentProvider = findViewById(R.id.btnContentProvider)
        btnIntent = findViewById(R.id.btnIntent)
        btnIntentForResult = findViewById(R.id.btnIntentForResult)
        btnAsyncTask = findViewById(R.id.btnAsyncTask)
        btnService = findViewById(R.id.btnService)
        btnBroadcast = findViewById(R.id.btnBroadcast)
        btnNotification = findViewById(R.id.btnNotification)
        btnMap = findViewById(R.id.btnMap)
        btnContentProvider.setOnClickListener(this)
        btnIntent.setOnClickListener(this)
        btnIntentForResult.setOnClickListener(this)
        btnService.setOnClickListener(this)
        btnAsyncTask.setOnClickListener(this)
        btnMap.setOnClickListener(this)
        btnBroadcast.setOnClickListener(this)
        btnNotification.setOnClickListener(this)
        btnRetrofit.setOnClickListener {
            val intent = Intent(this, RetrofitActivity::class.java)
            startActivity(intent)
        }
        btnCustomView.setOnClickListener {
            val intent = Intent(this, CustomViewActivity::class.java)
            startActivity(intent)
        }
        btnAnimation.setOnClickListener {
            val intent = Intent(this, AnimationActivity::class.java)
            startActivity(intent)
        }
        btnAddView.setOnClickListener {
            val intent = Intent(this, AddViewActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnContentProvider -> {
                val intent = Intent(this, ContentProviderActivity::class.java)
                startActivity(intent)
            }
            R.id.btnIntent -> {
                val intent = Intent(this, IntentActivity::class.java)
                val bundle = Bundle()
                bundle.putString("IntentValue", "Intent Example")
                intent.putExtra("BundlePackage", bundle)
                startActivity(intent)
            }
            R.id.btnIntentForResult -> {
                val intent = Intent(this, IntentActivity::class.java)
                val bundle = Bundle()
                bundle.putString("IntentValue", "Intent For Result Example")
                intent.putExtra("BundlePackage", bundle)
                startActivityForResult(intent, 202)
            }
            R.id.btnService -> {
                val intent = Intent(this, ServiceActivity::class.java)
                startActivity(intent)
            }
            R.id.btnAsyncTask -> {
                val intent = Intent(this, AsyncTaskActivity::class.java)
                startActivity(intent)
            }
            R.id.btnBroadcast -> {
                val intent = Intent(this, BroadCastRecieverActivity::class.java)
                startActivity(intent)
            }
            R.id.btnMap -> {
                val intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            }
            R.id.btnNotification -> {
                val intent = Intent(this, NotificationActivity::class.java)
                startActivity(intent)
            }
            R.id.btnRetrofit -> {
                val intent = Intent(this, RetrofitActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            202 -> {
                val resultData = data?.getStringExtra("IntentForResult")
                Toast.makeText(this, resultData, Toast.LENGTH_LONG).show()
            }
        }
    }
}
