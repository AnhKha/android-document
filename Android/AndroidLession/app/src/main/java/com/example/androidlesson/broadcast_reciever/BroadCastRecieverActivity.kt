package com.example.androidlesson.broadcast_reciever

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.example.androidlesson.R


class BroadCastRecieverActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.broadcast_activity_layout)
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onDestroy() {
        super.onDestroy()

    }
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            val messageToUser = "You are offline now." //TODO
            val mSnackBar = Snackbar.make(findViewById(R.id.llBroadcast), messageToUser, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar.show()
        } else {
            val messageToUser = "You are online now." //TODO
            val mSnackBar = Snackbar.make(findViewById(R.id.llBroadcast), messageToUser, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar.show()
        }
    }
}