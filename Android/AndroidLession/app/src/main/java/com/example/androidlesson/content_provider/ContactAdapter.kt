package com.example.androidlesson.content_provider

import android.support.v7.widget.RecyclerView
import android.view.View
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.androidlesson.R


class ContactAdapter(context: Context) : RecyclerView.Adapter<ContactAdapter.MyViewHolder>() {
    private var mListContacts: MutableList<Contact>? = null
    private var mContext: Context = context

    constructor (listContacts: MutableList<Contact>, context: Context) : this(context) {
        mListContacts = listContacts
        mContext = context
    }

    fun setListItems(listContacts: MutableList<Contact>) {
        mListContacts = listContacts
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_row_layout, p0, false))
    }

    override fun getItemCount(): Int {
        return mListContacts?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, p1: Int) {
        holder.txtTitle.text = mListContacts!![p1].name
        //holder.txtYear.text = mListContacts!![p1].number
        holder.llRow.setOnClickListener { Toast.makeText(mContext, p1.toString(), Toast.LENGTH_LONG).show() }


    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtTitle: TextView = view.findViewById(R.id.txtName)
        var txtYear: TextView = view.findViewById(R.id.txtYear)
        var llRow: ConstraintLayout = view.findViewById(R.id.llRow)
    }
}

