package com.example.androidlesson.glide

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.androidlesson.ImageRowBinding
import com.example.androidlesson.R


class ListDogImageAdapter(context: Context) : RecyclerView.Adapter<ListDogImageAdapter.MyViewHolder>() {
    private var mListDog: MutableList<String>? = null
    private var mContext: Context = context

    constructor (listDog: MutableList<String>, context: Context) : this(context) {
        mListDog = listDog
        mContext = context
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val itemRowBinding: ImageRowBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.image_row_layout, p0, false)
        itemRowBinding.imageUrl = "https://images.dog.ceo/breeds/hound-afghan/n02088094_1007.jpg"
        return MyViewHolder(itemRowBinding)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: MyViewHolder, p1: Int) {
        //holder.itemRowBinding.imageUrl = mListDog!![p1]

    }

    inner class MyViewHolder(var itemRowBinding: ImageRowBinding) : RecyclerView.ViewHolder(itemRowBinding.root) {

        init {
            itemRowBinding.executePendingBindings()
        }
    }
}
