package com.example.androidlesson.retrofit

import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.androidlesson.R
import com.example.androidlesson.glide.ListImageActivity
import com.example.androidlesson.glide.ListImageActivity.Companion.DOG_NAME

class ListDogAdapter(context: Context) : RecyclerView.Adapter<ListDogAdapter.MyViewHolder>() {
    private var mListDog: MutableList<String>? = null
    private var mContext: Context = context

    constructor (listDog: MutableList<String>, context: Context) : this(context) {
        mListDog = listDog
        mContext = context
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_row_layout, p0, false))
    }

    override fun getItemCount(): Int {
        return mListDog?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, p1: Int) {
        holder.txtTitle.text = mListDog!![p1]
        //holder.txtYear.text = mListContacts!![p1].number
        holder.llRow.setOnClickListener {
            val intent = Intent(mContext, ListImageActivity::class.java)
            intent.putExtra(DOG_NAME, mListDog!![p1])
            mContext.startActivity(intent)
        }


    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtTitle: TextView = view.findViewById(R.id.txtName)
        var txtYear: TextView = view.findViewById(R.id.txtYear)
        var llRow: ConstraintLayout = view.findViewById(R.id.llRow)
    }
}
