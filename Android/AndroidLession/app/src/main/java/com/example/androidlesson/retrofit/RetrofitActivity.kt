package com.example.androidlesson.retrofit

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.example.androidlesson.R
import kotlinx.android.synthetic.main.retrofit_activity_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RetrofitActivity : AppCompatActivity() {
    lateinit var apiService: APIService
    lateinit var listDogAdapter: ListDogAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.retrofit_activity_layout)
        apiService = ApiUtils().getAPIService()
        btnGetData.setOnClickListener { getData() }
    }

    private fun getData() {
        apiService.getListDog().enqueue(object : Callback<Model> {
            override fun onFailure(call: Call<Model>?, t: Throwable?) {
                Log.v("retrofit", "call failed")
            }

            override fun onResponse(call: Call<Model>?, response: Response<Model>?) {
                if (response?.body() != null && response.body() is Model) {
                    val model: Model = response.body()!!
                    model.message?.let { bindData(it) }
                }
            }

        })
    }

    private fun bindData(list: List<String>) {
        listDogAdapter = ListDogAdapter(list.toMutableList(), this)
        rcvListDog.layoutManager = LinearLayoutManager(this)
        rcvListDog.adapter = listDogAdapter
    }
}