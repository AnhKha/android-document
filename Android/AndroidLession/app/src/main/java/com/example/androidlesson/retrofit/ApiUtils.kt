package com.example.androidlesson.retrofit

class ApiUtils {

    private val BASE_URL = "https://dog.ceo/api/breed/"

    fun getAPIService(): APIService {
        return RetrofitClient().getClient(BASE_URL)!!.create(APIService::class.java)
    }
}