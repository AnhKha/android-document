package com.example.androidlesson.animation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.androidlesson.R
import kotlinx.android.synthetic.main.animation_activity.*


class AnimationActivity : AppCompatActivity(), Animation.AnimationListener {
    override fun onAnimationRepeat(animation: Animation?) {
        Toast.makeText(this, "Animation Stopped", Toast.LENGTH_SHORT).show()
    }

    override fun onAnimationEnd(animation: Animation?) {

    }

    override fun onAnimationStart(animation: Animation?) {

    }

    private lateinit var mAnimation: Animation
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.animation_activity)
        btnFadeIn.setOnClickListener { setAnimation(R.anim.fade_in) }
        btnZoomIn.setOnClickListener { setAnimation(R.anim.zoom_in) }
        btnFadeOut.setOnClickListener { setAnimation(R.anim.fade_out) }
    }

    private fun setAnimation(animation: Int) {
        mAnimation = AnimationUtils.loadAnimation(this, animation)
        mAnimation.setAnimationListener(this)
        imgAnimation.startAnimation(mAnimation)
    }
}