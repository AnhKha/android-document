package com.example.androidlesson.notification

import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import com.example.androidlesson.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService() {
    val CHANNEL_ID = "123"
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        var notification = remoteMessage.notification
        showSmallNotification(R.drawable.ic_launcher_foreground, notification!!.title!!, notification.body!!)
    }

    private fun showSmallNotification(smallIcon: Int, title: String,
                                      message: String) {
        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(smallIcon)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(12, builder.build())
        }
    }
}
