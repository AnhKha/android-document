package com.example.androidlesson.retrofit

import android.databinding.BaseObservable

class Model : BaseObservable() {
    var status: String = ""
    var message: List<String>? = null
}
