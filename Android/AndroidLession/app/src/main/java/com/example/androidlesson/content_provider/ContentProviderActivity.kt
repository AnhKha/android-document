package com.example.androidlesson.content_provider

import android.Manifest
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import com.example.androidlesson.R


class ContentProviderActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_provider_activity)
        val listPermissions: MutableList<String> = mutableListOf()
        listPermissions.add(Manifest.permission.READ_CONTACTS)
        ActivityCompat.requestPermissions(this, listPermissions.toTypedArray(), 1000)
        initLoader()
    }

    private fun initLoader() {

        var btnSearch = findViewById<Button>(R.id.btnSearch)
        val contactAdapter = ContactAdapter(this)
        val rcvContacts = findViewById<RecyclerView>(R.id.rcvContacts)
        rcvContacts.layoutManager = LinearLayoutManager(this)
        rcvContacts.adapter = contactAdapter
        btnSearch.setOnClickListener {
            var projectionFields = arrayOf(ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.NUMBER)
            val cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projectionFields, null,
                    null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
            if (cursor != null) {
                val listContacts = mutableListOf<Contact>()
                while (cursor.moveToNext()) {
                    val id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID))
                    val name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                    val number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    val contact = Contact(id, name, number)
                    listContacts.add(contact)
                }
                contactAdapter.setListItems(listContacts)
            }
            cursor!!.close()
        }
    }
}