package com.example.androidlesson.service

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import com.example.androidlesson.R
import android.content.Intent
import android.content.ServiceConnection
import com.example.androidlesson.service.ServiceExample.MyBinder
import android.os.IBinder
import android.content.ComponentName
import android.content.Context
import android.widget.Toast


class ServiceActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var btnStartService: Button
    private lateinit var btnStopService: Button
    private lateinit var btnStartBoundService: Button
    private lateinit var btnStopBoundService: Button
    private lateinit var btnSeek: Button
    private lateinit var connection: ServiceConnection
    private var isBound = false
    private lateinit var serviceExample: ServiceExample

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.service_activity_layout)
        btnStartService = findViewById(R.id.btnStartService)
        btnStopService = findViewById(R.id.btnStopService)
        btnStartBoundService = findViewById(R.id.btnStartBoundService)
        btnStopBoundService = findViewById(R.id.btnStopBoundService)
        btnSeek = findViewById(R.id.btnSeek)
        btnStartService.setOnClickListener(this)
        btnStopService.setOnClickListener(this)
        btnStartBoundService.setOnClickListener(this)
        btnStopBoundService.setOnClickListener(this)
        btnSeek.setOnClickListener(this)
        connection = object : ServiceConnection {

            // Phương thức này được hệ thống gọi khi kết nối tới service bị lỗi
            override fun onServiceDisconnected(name: ComponentName) {
                isBound = false
            }

            // Phương thức này được hệ thống gọi khi kết nối tới service thành công
            override fun onServiceConnected(name: ComponentName, service: IBinder) {
                val binder = service as MyBinder
                serviceExample = binder.getService() // lấy đối tượng MyService
                isBound = true
            }
        }
    }

    override fun onClick(v: View) {
        val intent = Intent(this, ServiceExample::class.java)
        intent.putExtra("DATA_SERVICE", "Data từ activity truyền sang Service")
        when (v.id) {
            R.id.btnStartService -> {
                startService(intent)
            }
            R.id.btnStopService -> {
                stopService(intent)
            }
            R.id.btnStartBoundService -> {
                // Bắt đầu một service sủ dụng bind
                bindService(intent, connection, Context.BIND_AUTO_CREATE)
                // Đối thứ ba báo rằng Service sẽ tự động khởi tạo
            }
            R.id.btnStopBoundService -> {
                // Nếu Service đang hoạt động
                if (isBound) {
                    // Tắt Service
                    unbindService(connection)
                    isBound = false
                }
            }
            R.id.btnSeek -> {
                // nếu service đang hoạt động
                if (isBound) {
                    // tua bài hát
                    serviceExample.fastForward()
                } else {
                    Toast.makeText(this, "Service chưa hoạt động", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}