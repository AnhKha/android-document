package com.example.androidlesson.custom_view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class ImageViewCustom : View {
    private lateinit var mCirclePaint: Paint
    private lateinit var mEyeAndMouthPaint: Paint
    private var mCenterX: Float = 0.0f
    private var mCenterY: Float = 0.0f
    private var mRadius: Float = 0.0f
    private var mArcBounds: RectF = RectF()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initPaints()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas != null) {
            canvas.drawCircle(mCenterX, mCenterY, mRadius, mCirclePaint)
            val eyeRadius = mRadius / 5f
            val eyeOffsetX = mRadius / 3f
            val eyeOffsetY = mRadius / 3f
            canvas.drawCircle(mCenterX - eyeOffsetX, mCenterY - eyeOffsetY, eyeRadius, mEyeAndMouthPaint)
            canvas.drawCircle(mCenterX + eyeOffsetX, mCenterY - eyeOffsetY, eyeRadius, mEyeAndMouthPaint)
            val mouthInset = mRadius / 3f
            mArcBounds.set(mouthInset, mouthInset, mRadius * 2 - mouthInset, mRadius * 2 - mouthInset)
            canvas.drawArc(mArcBounds, 45f, 90f, false, mEyeAndMouthPaint)
        }

    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mCenterX = w / 2f
        mCenterY = h / 2f
        mRadius = Math.min(w, h) / 2f
    }
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val w = View.MeasureSpec.getSize(widthMeasureSpec)
        val h = View.MeasureSpec.getSize(heightMeasureSpec)

        val size = Math.min(w, h)
        setMeasuredDimension(size, size)
    }

    private fun initPaints() {
        mCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        mCirclePaint.style = Paint.Style.FILL
        mCirclePaint.color = Color.YELLOW
        mEyeAndMouthPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        mEyeAndMouthPaint.style = Paint.Style.STROKE
        mEyeAndMouthPaint.strokeWidth = 16 * resources.displayMetrics.density
        mEyeAndMouthPaint.strokeCap = Paint.Cap.ROUND
        mEyeAndMouthPaint.color = Color.BLACK
    }
}