package com.example.androidlesson.custom_view

import android.content.Context
import android.content.res.TypedArray
import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log

import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.androidlesson.R

class HasakiImageView(context: Context, attrs: AttributeSet) : android.support.v7.widget.AppCompatImageView(context, attrs) {

    private var mMatchPresets: Boolean = false
    private var mCircleTransform: Boolean = false
    private var mSetSquare: Boolean = false

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.HasakiImageView, 0, 0)

        try {
            mMatchPresets = a.getBoolean(R.styleable.HasakiImageView_matchPresets, false)
            mCircleTransform = a.getBoolean(R.styleable.HasakiImageView_circleTransform, false)
            mSetSquare = a.getBoolean(R.styleable.HasakiImageView_setSquare, false)
        } finally {
            a.recycle()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // Set a square layout.
        if (mSetSquare) {
            val width = measuredWidth
            this.setMeasuredDimension(width, width)
        }
    }

    companion object {

        @BindingAdapter("imageUrl")
        @JvmStatic
        fun setImageUrl(view: HasakiImageView, imageUrl: String?) {
            if (view.mCircleTransform) {
                Glide.with(view.context)
                        .load(imageUrl)
                        .centerCrop()
                        .transform(ImageLoader.CircleTransform(view.context))
                        .into(view)
            } else {
                Glide.with(view.context)
                        .load(imageUrl)
                        .fitCenter()
                        .into(view)
            }
        }
    }
}